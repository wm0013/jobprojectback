//add node.js modul
const express = require('express')
var cors = require('cors')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
var mongoose = require('mongoose')
var fs = require('fs')
const path = require('path')

mongoose.connect("mongodb://localhost:27017/my_database", { useNewUrlParser: true, useUnifiedTopology: true });

//add router in here
const userRouter = require('./routes/users')
const companyRouter = require('./routes/company')
const loginRouter = require('./routes/login')
const jobPostingRouter = require('./routes/jobPosting')
const applyJobRouter = require('./routes/applyJob')

//check connect database
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function () {
  console.log('connectDB')
})

//use modul
const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

//set API 
//add API in here
app.use('/user', userRouter)
app.use('/company', companyRouter)
app.use('/login', loginRouter)
app.use('/jobPosting',jobPostingRouter)
app.use('/applyJob',applyJobRouter)

module.exports = app