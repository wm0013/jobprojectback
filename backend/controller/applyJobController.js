const applyJob = require('../models/applyJob')
const applyJobController = {
    async addApp(req, res) {
        const payload = req.body
        const applyJobs = new applyJob(payload)
        try {
            await applyJobs.save()
            res.json(applyJobs)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async updateApp(req, res) {
        const payload = req.body
        try {
            const applyJobs = await applyJob.updateOne({ _id: payload._id }, payload)
            res.json(applyJobs)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async deleteApp(req, res) {
        const { id } = req.params
        try {
            const applyJobs = await applyJob.deleteOne({ _id: id })
            res.json(applyJobs)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async getApp(req, res) {
        try {
            const applyJobs = await applyJob.find({})
            res.json(applyJobs)
        } catch (err) {
            res.status(500).send(err)
        }
    },
    async getAppID(req, res) {
        const { id } = req.params
        try {
            const applyJobs = await applyJob.findById(id)
            res.json(applyJobs)
        } catch (err) {
            res.status(500).send(err)
        }
    }
}
module.exports = applyJobController