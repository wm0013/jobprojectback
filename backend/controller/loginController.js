const Login = require('../models/login')
const loginController = {
  async addLogin(req,res){
    const payload = req.body
    const logins = new Login(payload)
    try{
        await logins.save()
         res.json(logins)
    }catch(err){
         res.status(500).send(err)
    }
},
async updateLogin(req, res) {
    const payload = req.body
    try {
      const logins = await Login.updateOne({ _id: payload._id }, payload)
      res.json(logins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteLogin(req, res) {
    const { id } = req.params
    try {
      const logins = await Login.deleteOne({ _id: id })
      res.json(logins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getLogin(req, res) {
    try {
      const logins = await Login.find({})
      res.json(logins)
      console.log(logins[0].username+" "+logins[0].password+" "+logins.length)
      

    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getLoginID(req, res) {
    const { id } = req.params
    try {
      const logins = await Login.findById(id)
      res.json(logins)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async checkLogin(req, res) {
    
  }
}
module.exports = loginController
