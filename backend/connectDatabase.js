const mongoose = require('mongoose')


mongoose.connect('mongodb://localhost:27017/my_database', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

// const kittySchema = new mongoose.Schema({
//   name: String
// });

// kittySchema.methods.speak = function () {
//   const greeting = this.name
//     ? "Meow name is " + this.name
//     : "I don't have a name";
//   console.log(greeting);
// }

// const Kitten = mongoose.model('Kitten', kittySchema);


// const silence = new Kitten({ name: 'Silence' });
// console.log(silence.name); // 'Silence'

// const fluffy = new Kitten({ name: 'fluffy' });
// fluffy.speak(); // "Meow name is fluffy"

// fluffy.save((err, fluffy) => {
//   if (err) return console.error(err);
//   fluffy.speak();
// });

// Kitten.find(function (err, kittens) {
//   if (err) return console.error(err);
//   console.log("test kittens")
//   console.log(kittens);
// })

// const jobPostingSchema = new mongoose.Schema({
//   namecompany: String,
//   position: String,
//   enlist: String,
//   welfare: String,
//   workTime: String,
//   gender : String,
//   educational : String,
//   salary : String,
//   description : String,
//   startDate : String,
//   endDate : String,
//   recruits : String
// });

// const JobPosting = mongoose.model('JobPosting', jobPostingSchema)

// const x = new JobPosting({
//   namecompany: 'E Commerce Solution Co., Ltd.',
//   position: 'Programmer / Web Programmer',
//   enlist: 'ผ่านเเล้ว',
//   welfare: 'ทำงานสัปดาห์ละ 5 วัน, มีเวลาการทำงานที่ยืดหยุ่น, ค่าตอบแทนพิเศษ, ประกันชีวิต, ประกันสุขภาพ, ค่าทำงานล่วงเวลา, เงินโบนัสตามผลงาน',
//   workTime: 'เต็มเวลา (Full Time)',
//   gender : 'ไม่จำกัดเพศ',
//   educational : 'ปริญญาตรี ขึ้นไป',
//   salary : 'THB 25,000 - THB 55,000 /เดือน (สามารถต่อรองได้)',
//   description : 'อายุ : 22 ปี, วุฒิการศึกษา : ปริญญาตรี, ประสบการณ์ : เขียน php 1 ปี, สกิลการเขียนโปรแกรม : php html mysql js',
//   startDate : '14/03/2021 00:00:00',
//   endDate : '31/03/2021 23:59:59',
//   recruits : '10 คน'
// });

// x.save((err, jobPosting)=>{
//   if (err) return console.error(err);

//   console.log('add database complete.')
// });

const applyJobSchema = new mongoose.Schema({
  nameSurname: String,
  position:String,
  address: String,
  experience: String,
  educational: String,
  salary: String,
  date : {
    type: Date,
    default: Date.now
  }
});

const applyJob = mongoose.model('applyJob', applyJobSchema);

const x = new applyJob({
  nameSurname: 'John Doe',
  position: 'Programmer',
  address: 'กรุงเทพ,เมือง',
  experience: '1 ปี',
  educational: 'ปริญญาตรี',
  salary: '-'
});

x.save((err, Seek) => {
  if (err) return console.error(err);

  console.log('add database complete.')
});