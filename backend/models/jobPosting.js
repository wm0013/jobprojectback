const mongoose = require('mongoose');
const jobPostingSchema = new mongoose.Schema({
  namecompany: String,
  position: String,
  enlist: String,
  welfare: String,
  workTime: String,
  gender : String,
  educational : String,
  salary : String,
  description : String,
  startDate : String,
  endDate : String,
  recruits : String
});

module.exports = mongoose.model('jobPosting', jobPostingSchema);