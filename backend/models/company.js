const mongoose = require('mongoose');
const companySchema = new mongoose.Schema({
  namecompany: String,
  email: String,
  ownerName: String,
  website: String,
  telphone: String,
  address: {
    housenum: String,
    road: String,
    subdistrict: String,
    district:String,
    province:String,
    postalNumber:String
  }
});

module.exports = mongoose.model('company', companySchema);
