const mongoose = require('mongoose')
const loginSchema = new mongoose.Schema({
  username: String,
  password: String,
  usertype: String
})

module.exports = mongoose.model('Login', loginSchema)
