const express = require('express')
const router = express.Router()
const companyController = require('../controller/companyController')

router.get('/hello',(req,res) =>{
  res.send("Hello MarsX Company")
})
router.get('/',companyController.getCom)
router.get('/:id',companyController.getComID)
router.post('/add',companyController.addCom)
router.put('/update/',companyController.updateCom)
router.delete('/delete/:id',companyController.deleteCom)

module.exports = router