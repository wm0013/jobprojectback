const express = require('express')
const router = express.Router()
const jobPostingController = require('../controller/jobPostController')

router.get('/',jobPostingController.getJob)
router.get('/:id',jobPostingController.getJobID)
router.post('/search',jobPostingController.search)
router.post('/add',jobPostingController.addJob)
router.put('/update/',jobPostingController.updateJob)
router.delete('/delete/:id',jobPostingController.deleteJob)

module.exports = router