const express = require('express')
const router = express.Router()
const applyJobController = require('../controller/applyJobController')

router.get('/',applyJobController.getApp)
router.get('/:id',applyJobController.getAppID)
router.post('/add',applyJobController.addApp)
router.put('/update/',applyJobController.updateApp)
router.delete('/delete/:id',applyJobController.deleteApp)

module.exports = router